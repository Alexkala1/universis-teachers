import {Component, HostListener, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ClassesService} from '../../services/classes.service';
import {NavigationEnd, Router} from '@angular/router';


@Component({
  selector: 'app-classes-all',
  templateUrl: './classes-all.component.html',
  styleUrls: ['./classes-all.component.scss'],
  providers: [ ClassesService]
})
export class ClassesAllComponent implements OnInit {

  public info1;
  public info2;
  public instDepar;
  public selectedIndex = 0;
  public isSrolling = true;
  private info4;

  constructor(private _context: AngularDataContext, private  classesService: ClassesService, private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) {
            element.scrollIntoView();
          }
        }
      }
    });
    this.classesService.getAllInstructors().then((res) => {
      this.info1 = res;
      this.instDepar = res.department.name;
    });

    this.classesService.getAllClasses().then((res) => {
      this.info2 = res;
      this.info4 = JSON.stringify(this.info2, null, 2);
    });
  }

  ngOnInit() {
  }
  setIndex(index: number) {
    this.selectedIndex = index;
    this.isSrolling = false;
  }

  @HostListener('scroll', ['$event'])
  scrollHandler(event) {
    return this.getScroll();
  }

  public getScroll( node = document.getElementById('inner__content') ): number {

    if ( !this.isSrolling ) {
      this.isSrolling = true;
      return null;
    }

    const currentScroll = node.scrollTop;
    const elements = document.getElementsByClassName('years');
    let fountActive = false;

    this.selectedIndex = -1;

    for (let j = 0; j < elements.length; j++) {
      const currentElement = elements.item(j);

      let currentOffSetTop = currentElement.getBoundingClientRect().top;
      if (currentOffSetTop < 0) {
        currentOffSetTop *= -1;
      }

    if (((currentElement.getBoundingClientRect().top > 0) || (currentElement.getBoundingClientRect().height - 10) >
        (node.getBoundingClientRect().top + currentOffSetTop)) && !fountActive) {

        this.selectedIndex = j;

        fountActive = true;
      }
    }
    return( currentScroll );
  }
}

